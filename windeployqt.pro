QT -= gui
CONFIG += c++11 console static
CONFIG -= app_bundle

win32: LIBS += -lshlwapi

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        elfreader.cpp \
        main.cpp \
        qmlutils.cpp \
        utils.cpp

HEADERS += \
    elfreader.h \
    paths.h \
    qmlutils.h \
    utils.h
