#ifndef PATHS_H
#define PATHS_H

#ifndef Q_OS_WIN
#define OBJDUMP "/usr/lib/mxe/usr/bin/x86_64-w64-mingw32.shared-objdump"
#define EXTRA_DLL_PATH "/usr/lib/mxe/usr/x86_64-w64-mingw32.shared/bin"
#endif

#endif // PATHS_H
